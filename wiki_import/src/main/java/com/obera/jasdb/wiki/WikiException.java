package com.obera.jasdb.wiki;

/**
 * Base exception when wiki source cannot be parsed or imported
 *
 * @author Renze de Vries
 */
public class WikiException extends Exception {
    public WikiException(String message, Throwable e) {
        super(message, e);
    }

    public WikiException(String message) {
        super(message);
    }
}
