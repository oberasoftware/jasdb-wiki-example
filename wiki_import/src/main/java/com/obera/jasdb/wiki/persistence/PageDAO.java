package com.obera.jasdb.wiki.persistence;

import com.obera.jasdb.wiki.model.WikiPage;

import java.util.List;
import java.util.Map;

/**
 * The base persistence class for storing and retrieving wiki pages
 *
 * @author Renze de Vries
 */
public interface PageDAO {
    /**
     * Store a wiki page
     * @param page The wiki page to store
     * @throws WikiStorageException If unable to store the wiki page
     */
    void storePage(WikiPage page) throws WikiStorageException;

    /**
     * Finds all wiki pages for the given wiki property / value combinations
     * @param propertyValues The map of properties to search for with the required values
     * @return A list of found wiki pages or empty list if none found
     * @throws WikiStorageException If unable to search the wiki pages
     */
    List<WikiPage> findPages(Map<String, String> propertyValues) throws WikiStorageException;

    /**
     * Finds all wiki pages for the given wiki property / value combinations with a paged result, starting
     * from begin and returning a limit amount of results.
     *
     * @param propertyValues The map of properties to search for with the required values
     * @return A list of found wiki pages or empty list if none found
     * @throws WikiStorageException If unable to search the wiki pages
     */
    List<WikiPage> findPages(Map<String, String> propertyValues, int begin, int limit) throws WikiStorageException;

    /**
     * Retrieves a count of all the wiki pages for the given property / value combination
     * @param propertyValues The map of properties to search for with the required values
     * @return The count for the wiki pages, will be 0l if none found
     * @throws WikiStorageException If unable to search the wiki pages
     */
    Long countPages(Map<String, String> propertyValues) throws WikiStorageException;
}
