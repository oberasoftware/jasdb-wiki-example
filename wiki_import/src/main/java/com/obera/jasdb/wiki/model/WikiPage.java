package com.obera.jasdb.wiki.model;

import nl.renarj.core.utilities.StringUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents all extracted wiki data
 *
 * @author Renze de Vries
 */
public class WikiPage {
    private String title;
    private String ns;
    private String id;
    private boolean redirect = false;
    private boolean minor = false;
    private String restrictions;

    private String revisionId;
    private String revisionTimestamp;
    private String contributorUserName;
    private String contributorId;
    private String contributorIp;
    private String comment;

    private String firstParagraph;
    private String rawText;

    private Set<String> categories = new HashSet<String>();
    private Set<String> links = new HashSet<String>();
    private Set<String> languages = new HashSet<String>();

    public String getContributorIp() {
        return contributorIp;
    }

    public void setContributorIp(String contributorIp) {
        this.contributorIp = contributorIp;
    }

    public String getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(String restrictions) {
        this.restrictions = restrictions;
    }

    public boolean isRedirect() {
        return redirect;
    }

    public void setRedirect(boolean redirect) {
        this.redirect = redirect;
    }

    public boolean isMinor() {
        return minor;
    }

    public void setMinor(boolean minor) {
        this.minor = minor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNs() {
        return ns;
    }

    public void setNs(String ns) {
        this.ns = ns;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRevisionId() {
        return revisionId;
    }

    public void setRevisionId(String revisionId) {
        this.revisionId = revisionId;
    }

    public String getRevisionTimestamp() {
        return revisionTimestamp;
    }

    public void setRevisionTimestamp(String revisionTimestamp) {
        this.revisionTimestamp = revisionTimestamp;
    }

    public String getContributorUserName() {
        return contributorUserName;
    }

    public void setContributorUserName(String contributorUserName) {
        this.contributorUserName = contributorUserName;
    }

    public String getContributorId() {
        return contributorId;
    }

    public void setContributorId(String contributorId) {
        this.contributorId = contributorId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFirstParagraph() {
        return firstParagraph;
    }

    public void setFirstParagraph(String firstParagraph) {
        this.firstParagraph = firstParagraph;
    }

    public String getRawText() {
        return rawText;
    }

    public void setRawText(String rawText) {
        this.rawText = rawText;
    }

    public Set<String> getLinks() {
        return links;
    }

    public void setLinks(Set<String> links) {
        this.links = links;
    }

    public void addLink(String link) {
        if(StringUtils.stringNotEmpty(link)) {
            this.links.add(link);
        }
    }

    private String getSummaryText(int maxSummary) {
        if(rawText != null && !rawText.isEmpty()) {
            return rawText.substring(0, rawText.length() > maxSummary ? maxSummary : rawText.length());
        } else {
            return null;
        }
    }

    public void addCategory(String category) {
        if(StringUtils.stringNotEmpty(category)) {
            this.categories.add(category);
        }
    }

    public Set<String> getCategories() {
        return categories;
    }

    public void setCategories(Set<String> categories) {
        this.categories = categories;
    }

    public Set<String> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<String> languages) {
        this.languages = languages;
    }

    public void addLanguage(String language) {
        this.languages.add(language);
    }

    @Override
    public String toString() {
        return "Page{" +
                "title='" + title + '\'' +
                ", ns='" + ns + '\'' +
                ", id='" + id + '\'' +
                ", redirect=" + redirect +
                ", minor=" + minor +
                ", restrictions='" + restrictions + '\'' +
                ", revisionId='" + revisionId + '\'' +
                ", revisionTimestamp='" + revisionTimestamp + '\'' +
                ", contributorUserName='" + contributorUserName + '\'' +
                ", contributorId='" + contributorId + '\'' +
                ", contributorIp='" + contributorIp + '\'' +
                ", comment='" + comment + '\'' +
                ", text='" + getSummaryText(20) + '\'' +
                '}';
    }
}
