package com.obera.jasdb.wiki;

import com.obera.jasdb.wiki.model.WikiPage;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.FlushTemplates;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.MediaWikiParser;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.MediaWikiParserFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;

/**
 * Streaming decodes the wikimedia XML dump and uses the JWPL wikimedia parser to extract keydata
 *
 * @author Renze de Vries
 */
public class StreamingWikiParser implements Iterator<WikiPage>, Iterable<WikiPage> {
    private static final Logger log = LoggerFactory.getLogger(StreamingWikiParser.class);
    private static MediaWikiParserFactory pf = new MediaWikiParserFactory();
    static {
        pf.getCategoryIdentifers().add("Categorie");
        pf.setTemplateParserClass(FlushTemplates.class);
        pf.setShowImageText(false);
        pf.setShowMathTagContent(false);
    }

    private MediaWikiParser parser = pf.createParser();

    private static XMLInputFactory inputFactory = XMLInputFactory.newFactory();
    private XMLStreamReader streamReader;

    public StreamingWikiParser(File path) throws WikiException {
        try {
            streamReader = inputFactory.createXMLStreamReader(new FileInputStream(path));
        } catch(FileNotFoundException e) {
            throw new WikiException("Wiki input file could not be found", e);
        } catch (XMLStreamException e) {
            throw new WikiException("Wiki input file could not be parsed", e);
        }
    }

    @Override
    public Iterator<WikiPage> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        try {
            return searchForTag("page");
        } catch(XMLStreamException e) {
            log.error("Unable to read wiki xml stream", e);
            return false;
        }
    }

    @Override
    public WikiPage next() {
        try {
            return decodePage(streamReader);
        } catch(XMLStreamException e) {
            log.error("Unable to read wiki xml stream", e);
            throw new RuntimeException("Invalid xml stream");
        }
    }

    @Override
    public void remove() {}

    /**
     * Check if there is a next page in the stream
     * @param tag The tag to search for
     * @return True if there is a next page, false if not
     * @throws XMLStreamException If unable to read the stream
     */
    private boolean searchForTag(String tag) throws XMLStreamException {
        //First check if we are already there
        if(streamReader.getEventType() == XMLStreamReader.START_ELEMENT && streamReader.getLocalName().equals(tag)) {
            return true;
        } else {
            //search for the next tag
            while(streamReader.getEventType() != XMLStreamReader.END_DOCUMENT && (streamReader.getEventType() != XMLStreamReader.START_ELEMENT  || !streamReader.getLocalName().equals(tag))) {
                streamReader.next();
            }

            //check if we are not at the end
            if(streamReader.getEventType() != XMLStreamReader.END_DOCUMENT && streamReader.getLocalName().equals(tag)) {
                return true;
            } else {
                return false;
            }
        }
    }

    private WikiPage decodePage(XMLStreamReader streamReader) throws XMLStreamException {
        WikiPage page = new WikiPage();
        searchForTag("page");

        page.setTitle(getValue("title", true, true));
        page.setId(getValue("id", true, true));

        if(streamReader.getLocalName().equals("revision")) {
            streamReader.nextTag();
            page.setRevisionId(getValue("id", true, true));
            page.setRevisionTimestamp(getValue("timestamp", true, true));
            page.setRawText(getValue("text", true, true));

//            //This part parsed the media wiki contents for categories
//            ParsedPage parsedPage = parser.parse(page.getRawText());
//            if(parsedPage != null && parsedPage.getCategories() != null) {
//                List<Link> categories = parsedPage.getCategories();
//                for(Link category : categories) {
//                    page.addCategory(getCategory(category.getText()));
//                }
//            }
        }

        return page;
    }

    /**
     * Simple method to clean up the category text a bit
     * @param categoryText The text to clean up
     * @return The cleaned up category string
     */
    private String getCategory(String categoryText) {
        return categoryText.replace("Categorie:", "").replace("categorie:", "");
    }

    private String getValue(String tag, boolean mandatory, boolean advance) throws XMLStreamException {
        searchForTag(tag);
        if(streamReader.getLocalName().equals(tag)) {
            String text = streamReader.getElementText();
            if(advance) {
                streamReader.nextTag();
            }
            return text;
        } else {
            if(mandatory) {
                throw  new RuntimeException("Invalid wikimedia format expect tag: " + tag + " but was: " + streamReader.getLocalName());
            } else {
                return null;
            }
        }

    }
}
