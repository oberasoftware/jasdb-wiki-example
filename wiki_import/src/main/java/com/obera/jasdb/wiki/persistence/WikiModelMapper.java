package com.obera.jasdb.wiki.persistence;

import com.obera.jasdb.wiki.model.WikiPage;
import nl.renarj.jasdb.api.SimpleEntity;

import java.util.Set;

/**
 * @author Renze de Vries
 */
public class WikiModelMapper {
    private static final String KEYWORDS = "keywords";
    private static final String LINKS = "links";
    private static final String LANGUAGES = "languages";
    private static final String COMMENT = "comment";
    private static final String ID = "id";
    private static final String NS = "ns";
    private static final String TITLE = "title";
    private static final String FIRST_PARAGRAPH = "firstParagraph";

    /**
     * Maps from the wiki page model into the JasDB SimpleEntity model
     * @param wikiPage The wiki page to map
     * @return The mapped JasDB entity
     */
    public static SimpleEntity toEntity(WikiPage wikiPage) {
        SimpleEntity entity = new SimpleEntity();
        entity.addProperty(TITLE, wikiPage.getTitle());
        entity.addProperty(NS, wikiPage.getNs());

        entity.addProperty(ID, wikiPage.getId());
        Boolean isRedirect = wikiPage.isRedirect();
        entity.addProperty("redirect", isRedirect.toString());
        entity.addProperty("restrictions", wikiPage.getRestrictions());
        entity.addProperty("revisionid", wikiPage.getRevisionId());
        entity.addProperty("revisiontimestamp", wikiPage.getRevisionTimestamp());
        entity.addProperty("contributorusername", wikiPage.getContributorUserName());
        entity.addProperty("contributorid", wikiPage.getContributorId());
        entity.addProperty("contributorip", wikiPage.getContributorIp());
        entity.addProperty("raw", wikiPage.getRawText());
        Boolean isMinor = wikiPage.isMinor();
        entity.addProperty("minor", isMinor.toString());
        entity.addProperty(COMMENT, wikiPage.getComment());
        if(wikiPage.getFirstParagraph() != null) {
            entity.addProperty(FIRST_PARAGRAPH, wikiPage.getFirstParagraph());
        }
        addLinks(entity, KEYWORDS, wikiPage.getCategories());
        addLinks(entity, LINKS, wikiPage.getLinks());
        addLinks(entity, LANGUAGES, wikiPage.getLanguages());

        return entity;
    }

    /**
     * Simple mapper method that maps from JasDB entity into the WikiPage model
     *
     * @param entity The JasDB entity
     * @return The mapped wiki page
     */
    public static WikiPage fromEntity(SimpleEntity entity) {
        WikiPage wikiPage = new WikiPage();
        wikiPage.setTitle((String)entity.getValue(TITLE));
        wikiPage.setNs((String)entity.getValue(NS));
        wikiPage.setId((String)entity.getValue(ID));
        wikiPage.setComment((String)entity.getValue(COMMENT));
        if(entity.hasProperty(KEYWORDS)) {
            for(Object value : entity.getValues(KEYWORDS)) {
                wikiPage.addCategory(value.toString());
            }
        }
        if(entity.hasProperty(LINKS)) {
            for(Object value : entity.getValues(LINKS)) {
                wikiPage.addLink(value.toString());
            }
        }
        if(entity.hasProperty(LANGUAGES)) {
            for(Object value : entity.getValues(LANGUAGES)) {
                wikiPage.addLanguage(value.toString());
            }
        }
        if(entity.hasProperty(FIRST_PARAGRAPH)) {
            wikiPage.setFirstParagraph(entity.getValue(FIRST_PARAGRAPH).toString());
        }
        return wikiPage;
    }

    /**
     * Helper method to add the links to the entity
     *
     * @param entity The entity
     * @param propertyName THe name of the property to put the links under
     * @param links The links
     */
    private static void addLinks(SimpleEntity entity, String propertyName, Set<String> links) {
        if(!links.isEmpty()) {
            entity.addProperty(propertyName, links.toArray(new String[links.size()]));
        }
    }
}
