package com.obera.jasdb.wiki;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * @author Renze de Vries
 */
public class WikiImporter {
    private static final Logger LOG = LoggerFactory.getLogger(WikiImporter.class);

    private String wikiSourcePath;

    public WikiImporter(String wikiSourcePath) {
        this.wikiSourcePath = wikiSourcePath;
    }

    public void startImport() throws WikiException {
        File importFile = new File(wikiSourcePath);
        if(importFile.exists()) {
            LOG.info("Starting import of: {}", wikiSourcePath);
            WikiEntryStorer entryStorer = new WikiEntryStorer();
            entryStorer.importAndStore(importFile);
        } else {
            throw new WikiException("Unable to parse wiki source, file: " + wikiSourcePath + " cannot be found");
        }
    }

    public static void main(String[] args) {
        if(args.length == 1) {
            WikiImporter importer = new WikiImporter(args[0]);
            try {
                importer.startImport();
            } catch(WikiException e) {
                LOG.error("Unable to import", e);
            }
        } else {
            LOG.info("Please specify wiki source path as argument");
        }
    }
}
