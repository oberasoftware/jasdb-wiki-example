package com.obera.jasdb.wiki.persistence;

import com.obera.jasdb.wiki.WikiException;

/**
 * @author Renze de Vries
 */
public class WikiStorageException extends WikiException {
    public WikiStorageException(String message, Throwable e) {
        super(message, e);
    }

    public WikiStorageException(String message) {
        super(message);
    }
}
