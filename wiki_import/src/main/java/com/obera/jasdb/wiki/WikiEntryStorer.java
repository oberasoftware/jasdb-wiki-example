package com.obera.jasdb.wiki;

import com.obera.jasdb.wiki.model.WikiPage;
import com.obera.jasdb.wiki.persistence.PageDAO;
import com.obera.jasdb.wiki.persistence.WikiStorageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * @author Renze de Vries
 */
public class WikiEntryStorer {
    private static final Logger LOG = LoggerFactory.getLogger(WikiEntryStorer.class);
    private static final int INTERVAL_REPORT = 10000;

    private ApplicationContext applicationContext;

    public WikiEntryStorer() {
        applicationContext = new ClassPathXmlApplicationContext("/META-INF/spring/service-context.xml");
    }

    public void importAndStore(File importFile) throws WikiException {
        PageDAO pageDAO = applicationContext.getBean(PageDAO.class);

        long count = 0;
        long start = System.currentTimeMillis();
        StreamingWikiParser wikiParser = new StreamingWikiParser(importFile);
        LOG.info("Starting reading wiki input");
        while(wikiParser.hasNext()) {
            count++;

            WikiPage page = wikiParser.next();
            try {
                pageDAO.storePage(page);
            } catch(WikiStorageException e) {
                LOG.warn("Could not store wiki page, skipping: {}", e.getMessage());
            }

            if(count % INTERVAL_REPORT == 0) {
                long end = System.currentTimeMillis();
                LOG.info("Parsed and Stored: {} pages in: {} seconds", count, TimeUnit.SECONDS.convert((end - start), TimeUnit.MILLISECONDS));
            }
        }
        long end = System.currentTimeMillis();
        LOG.info("Finished parsing and storing: {} wiki documents in: {} seconds", count, TimeUnit.SECONDS.convert((end - start), TimeUnit.MILLISECONDS));
    }
}
