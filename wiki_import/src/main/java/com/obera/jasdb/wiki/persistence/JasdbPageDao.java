package com.obera.jasdb.wiki.persistence;

import com.obera.jasdb.wiki.model.WikiPage;
import nl.renarj.jasdb.api.DBSession;
import nl.renarj.jasdb.api.SimpleEntity;
import nl.renarj.jasdb.api.model.EntityBag;
import nl.renarj.jasdb.api.query.BlockType;
import nl.renarj.jasdb.api.query.QueryBuilder;
import nl.renarj.jasdb.api.query.QueryExecutor;
import nl.renarj.jasdb.api.query.QueryResult;
import nl.renarj.jasdb.core.SimpleKernel;
import nl.renarj.jasdb.core.exceptions.JasDBException;
import nl.renarj.jasdb.core.exceptions.JasDBStorageException;
import nl.renarj.jasdb.index.keys.types.StringKeyType;
import nl.renarj.jasdb.index.search.IndexField;
import nl.renarj.jasdb.rest.client.RestDBSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Renze de Vries
 */
@Component
@Qualifier("jasdb")
public class JasdbPageDao implements PageDAO, DisposableBean {
    private static final Logger LOG = LoggerFactory.getLogger(JasdbPageDao.class);

    private static final String WIKI_BAG = "wikidata";

    private DBSession session;
    private EntityBag bag;

    public JasdbPageDao() throws JasDBStorageException {
        session = new RestDBSession("default", "localhost", 7050);
//        session = new LocalDBSession();
        this.bag = session.createOrGetBag(WIKI_BAG);
        bag.ensureIndex(new IndexField("keywords", new StringKeyType(400)), false);
        bag.ensureIndex(new IndexField("title", new StringKeyType(400)), false);
    }

    @Override
    public void destroy() {
        try {
            SimpleKernel.shutdown();
        } catch(JasDBException e) {
            LOG.error("Unable to shutdown JasdDB cleanly", e);
        }
    }

    @Override
    public void storePage(WikiPage page) throws WikiStorageException {
        try {
            bag.addEntity(WikiModelMapper.toEntity(page));
        } catch(JasDBStorageException e) {
            throw new WikiStorageException("Unable to store wiki entry in JasDB", e);
        }
    }

    @Override
    public List<WikiPage> findPages(Map<String, String> propertyValues) throws WikiStorageException {
        return findPages(propertyValues, -1, -1);
    }

    @Override
    public List<WikiPage> findPages(Map<String, String> propertyValues, int begin, int limit) throws WikiStorageException {
        QueryResult result = searchWikiPages(propertyValues, begin, limit);
        List<WikiPage> wikiPages = new LinkedList<WikiPage>();
        for(SimpleEntity entity : result) {
            wikiPages.add(WikiModelMapper.fromEntity(entity));
        }
        return wikiPages;
    }

    @Override
    public Long countPages(Map<String, String> propertyValues) throws WikiStorageException {
        QueryResult result = searchWikiPages(propertyValues, -1, -1);
        try {
            return result.size();
        } finally {
            result.close();
        }
    }

    /**
     * Execute the actual JasDB search, build the query and set the paging
     * @param propertyValues The property / value to search for
     * @param begin The begin item
     * @param limit The maximum amount of items
     * @return The query result
     * @throws WikiStorageException If unable to execute the query
     */
    private QueryResult searchWikiPages(Map<String, String> propertyValues, int begin, int limit) throws WikiStorageException {
        QueryBuilder queryBuilder = QueryBuilder.createBuilder(BlockType.OR);
        for(Map.Entry<String, String> condition : propertyValues.entrySet()) {
            queryBuilder.field(condition.getKey()).value(condition.getValue());
        }

        try {
            QueryExecutor queryExecutor = bag.find(queryBuilder);
            if(begin > 0 && limit > 0) {
                queryExecutor.paging(begin, limit);
            } else if(limit > 0) {
                queryExecutor.limit(limit);
            }
            return queryExecutor.execute();
        } catch(JasDBStorageException e) {
            LOG.error("Unable to search for wiki pages", e);
            throw new WikiStorageException("Unable to retrieve wiki pages", e);
        }
    }

}
