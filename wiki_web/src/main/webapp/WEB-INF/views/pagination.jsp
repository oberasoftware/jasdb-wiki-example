<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div >
    <div class="center-text">
        <span class="label label-info">Nr of results: <c:out value="${total}"/> found in <c:out value="${time}"/> seconds</span>
    </div>
    <div class="pagination pagination-centered">
        <ul>
            <c:forEach var="pageLink" items="${pageLinks}">
                <c:choose>
                    <c:when test="${pageLink.pageNumber == currentPage}">
                        <li class="active">
                    </c:when>
                    <c:otherwise>
                        <li>
                    </c:otherwise>
                </c:choose>
                <c:set value="${pageLink.pageNumber}" var="offset"/>
                <a href="search?q=<%= request.getParameter("q") %>&offset=<c:out value="${offset}"/>">
                <c:out value="${pageLink.pageNumber + 1}"/>
                </a>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>
