<html>
<head>
    <title>Wiki Search</title>
    <link href="../../bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../../bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label" for="appendedInputButton">Search wiki text</label>
            <div class="controls">
                <div class="input-append">
                    <input class="span5" id="appendedInputButton" size="16" type="text"><button class="btn" type="button">Go!</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <h1>Unable to execute search ${errorMessage}</h1>
    </div>
</div>
</body>
</html>
