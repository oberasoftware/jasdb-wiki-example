<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html>
<head>
    <title>Wiki Search</title>
    <link href="../../bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../../bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span1">&nbsp;</div>
        <div class="span10">
            <div class="control-group">
                <label class="control-label" for="searchWiki">Search wiki text</label>
                <form method="GET" action="/search">
                    <div class="controls">
                        <div class="input-append">
                            <c:choose>
                                <c:when test="${not empty param.q}">
                                    <input class="span5" id="searchWiki" size="16" name="q" type="text" value="<%= request.getParameter("q") %>"><button class="btn" type="submit">Go!</button>
                                </c:when>
                                <c:otherwise>
                                    <input class="span5" id="searchWiki" size="16" name="q" type="text" value=""><button class="btn" type="submit">Go!</button>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span1">&nbsp;</div>
        <div class="span10">
            <c:choose>
                <c:when test="${fn:length(results) > 0}">
                    <%--<span class="label label-info">Nr of results: <c:out value="${total}"/> found in <c:out value="${time}"/> seconds</span>--%>
                    <jsp:include page="pagination.jsp"/>
                    <div class="well">
                        <c:forEach var="wikiEntry" items="${results}">
                            <div class="row-fluid">
                                <strong><a href="${wikiEntry.url}"><span>${wikiEntry.title}</span></a></strong>
                                <blockquote>
                                    <c:if test="${wikiEntry.paragrah != null}">
                                        <p><c:out value="${wikiEntry.paragrah}"/></p>
                                    </c:if>
                                </blockquote>
                                <c:if test="${wikiEntry.keywords.size() > 0}">
                                    <ul class="breadcrumb">
                                        <c:forEach var="keyword" items="${wikiEntry.keywords}" varStatus="loop">
                                            <li>
                                                <a href="/search?q=${keyword}">
                                                    <c:out value="${keyword}"/>
                                                </a>
                                            </li>
                                            <c:if test="${!loop.last}">
                                                <span class="divider">/</span>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </c:if>
                            </div>
                            <br/>
                        </c:forEach>
                    </div>
                    <jsp:include page="pagination.jsp"/>
                </c:when>
                <c:otherwise>
                    <span class="label label-info">No results found</span>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
</body>
</html>
