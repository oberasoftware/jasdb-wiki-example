package com.obera.jasdb.wiki.web;

/**
 * @author renarj
 */
public class PageLink {
    private long pageNumber;
    public PageLink(long pageNumber) {
        this.pageNumber = pageNumber;
    }

    public long getPageNumber() {
        return pageNumber;
    }
}
