package com.obera.jasdb.wiki.web;

import com.obera.jasdb.wiki.model.WikiPage;
import com.obera.jasdb.wiki.persistence.PageDAO;
import com.obera.jasdb.wiki.persistence.WikiStorageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author renarj
 */
@Controller
@RequestMapping(value = "/search")
public class SearchController {
    private static final Logger LOG = LoggerFactory.getLogger(SearchController.class);
    private static final int PAGING_SIZE = 10;
    private static final int MAXLENGTH_PARAGRAPH = 300;

    @Autowired
    private PageDAO pageDAO;

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public String doPost() {
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String searchWiki(@RequestParam String q, @RequestParam(required = false) Integer offset, Model model) {
        try {
            long count = pageDAO.countPages(params(q, q));

            long start = System.currentTimeMillis();
            int begin = offset != null && offset > 0 ? offset : 0;
            List<WikiPage> wikiPages = pageDAO.findPages(params(q, q), begin * PAGING_SIZE, PAGING_SIZE);
            long end = System.currentTimeMillis();

            long passed = (end - start);
            double seconds = (double)passed / (double)1000;

            model.addAttribute("total", count);
            model.addAttribute("currentPage", begin);
            model.addAttribute("pageLinks", generatePageLinks(count));
            model.addAttribute("results", mapToWikiEntries(wikiPages));
            model.addAttribute("time", seconds);
        } catch(WikiStorageException e) {
            LOG.error("", e);
            model.addAttribute("errorMessage", e.getMessage());
            return "error";
        }

        return "index";
    }

    /**
     * Small helper to generate the query param map
     * @param title The title
     * @param keyword The keyword we are searching for
     * @return The parameter map
     */
    private Map<String, String> params(String title, String keyword) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("keywords", keyword);
        params.put("title", title);
        return params;
    }

    /**
     * Generate the page links based on the total count
     * @param totalSize The count
     * @return The list of page links
     */
    private List<PageLink> generatePageLinks(long totalSize) {
        List<PageLink> generatedPageLinks = new ArrayList<PageLink>();
        for(long i=0; i<totalSize / PAGING_SIZE; i++) {
            generatedPageLinks.add(new PageLink(i));
        }

        return generatedPageLinks;
    }

    /**
     * Map the internal domain model to the web model
     * @param wikiPages The wiki pages
     * @return The list of web ready pages
     */
    private List<WikiEntry> mapToWikiEntries(List<WikiPage> wikiPages) {
        List<WikiEntry> wikiEntries = new LinkedList<WikiEntry>();
        for(WikiPage wikiPage : wikiPages) {
            String title = wikiPage.getTitle();
            String url = "http://nl.wikipedia.org/wiki/" + title.replaceAll(" ", "_");
            WikiEntry entry = new WikiEntry(title, url);
            setAndAbbreviateParagraph(entry, wikiPage.getFirstParagraph());
            entry.setKeywords(wikiPage.getCategories());
            wikiEntries.add(entry);
        }
        return wikiEntries;
    }

    private void setAndAbbreviateParagraph(WikiEntry entry, String paragraph) {
        if(paragraph != null) {
            entry.setParagrah(paragraph.substring(0, paragraph.length() > MAXLENGTH_PARAGRAPH ? MAXLENGTH_PARAGRAPH : paragraph.length()) + "....");
        }
    }
}
