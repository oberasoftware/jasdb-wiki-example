package com.obera.jasdb.wiki.web;

import java.util.HashSet;
import java.util.Set;

/**
 * @author renarj
 */
public class WikiEntry {
    private String title;
    private String url;
    private Set<String> keywords;
    private String paragrah;

    public WikiEntry(String title, String url) {
        this.title = title;
        this.url = url;
        this.keywords = new HashSet<String>();
    }

    public void addKeyword(String keyword) {
        this.keywords.add(keyword);
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public Set<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<String> keywords) {
        this.keywords = keywords;
    }

    public String getParagrah() {
        return paragrah;
    }

    public void setParagrah(String paragrah) {
        this.paragrah = paragrah;
    }
}
